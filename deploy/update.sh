#!/bin/bash
echo "------------- FITIN PROJECT LANDING PAGE UPDATING, DO NOT INSTALL NPM PACKAGES SCRIPT  -------------"
echo "[-] check src folder"
#git reset --hard
PM2_NAME="project-landing-page"
NAME="src"
W="${PWD}"
D="${PWD}/${NAME}"
TEMP="${PWD}/${NAME}@temp"
PUBLIC="${PWD}/${NAME}/public"
# LINKDIR="/var/www/static.fitin3d.com/ecom-v3"
echo "$D"
if [  -d "$D" ]; then
  echo "stop PM2" 
  pm2 stop "$PM2_NAME"
fi

if [ ! -d "$TEMP" ]; then
  mkdir -p $TEMP
else 
	echo "[-] clean and reset"
 	rm -rf $TEMP	
 	mkdir -p $TEMP
fi


cd ${TEMP}
echo "[-] get src from git, please enter your password."
git clone git@bitbucket.org:anhtufitinvn/project-landingpage.git "."
git checkout develop
echo "[-] copy node_modules"
# move folder node_modules to new src
mv $D/node_modules $TEMP

sleep 1
if [  -d "$D" ]; then
 	rm -rf $D
fi

sleep 1
mv $TEMP $D
sleep 2

# create a softlink
# echo "[-] create nginx static directory"
# if [ ! -d "$LINKDIR" ]; then
#   mkdir -p $LINKDIR
#   echo "[-] change owner to nginx user"
#   chown -R nginx:nginx $LINKDIR
# fi

# remove old link before update new link
# echo "[-] unlink"
# rm -rf $LINKDIR/public
# link /var/www/static.fitin3d.com/ecom-v3 => ./public/
# echo "[-] create softlink"
# ln -s $D/public $LINKDIR/public
# To verify new soft link run
# echo "[-] verify new soft link run"
# ls -l $D/public $LINKDIR/public
# change own to nginx user
echo "[-] change owner to nginx user"
chown -R nginx:nginx $D/public

sleep 3

pm2 start "$D/process-dev.json" --update-env
echo DONE
