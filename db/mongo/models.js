(function () {
    const mongoose = require('mongoose');
    var {AutoIncrement, HomePageSchema} = require('./schemas/schemas-index');
    module.exports = {
        AutoIncrement : mongoose.model('AutoIncrement', AutoIncrement),
        HomePageModel: mongoose.model('E_Pages', HomePageSchema)
    };
})();