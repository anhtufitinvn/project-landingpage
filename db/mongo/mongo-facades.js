(function () {
    let {ClientInboxModel, HomePageModel} = require('./models');

    let {debug, logger} = require('../../core/logger');
    let Facades = {};

    Facades.addClientInbox = async function(payload) {
        try {
            let model = new ClientInboxModel(payload);
            let result = await model.save();
            // debug(result);
            return result._id;
        } catch (e) {
            logger.error(e);
            return null;
        }
    };

    Facades.getClientInbox = function(email) {
        return new Promise((resolve, reject) => {
            let query = ClientInboxModel.find({email : email || null});
            query.limit(100);
            query.sort({createdAt: -1});
            query.collation({locale: "en_US", numericOrdering: true});
            query.exec((err, result) => {
                if (err) {
                    reject(err);
                    return;
                }

                let list = result.filter(e => {
                    return e["_doc"]["image_paths"].length > 0;
                });

                resolve(list);
            });
        });
    };

    Facades.getConsultantData = function(signature) {
        return new Promise((resolve, reject) => {
            let query = ClientInboxModel.findOne({"cart.cart_sign": signature });
            query.collation({locale: "en_US", numericOrdering: true});
            query.exec((err, result) => {
                if (err) {
                    reject(err);
                    return;
                }
                if(result) {
                    resolve(result["_doc"]);
                } else {
                    resolve(null);
                }
            });
        });
    };

    Facades.updateCartSign = function(signature, newSignature) {
        return new Promise((resolve, reject) => {
            let query = ClientInboxModel.updateOne({"cart.cart_sign": signature }, { "cart.cart_sign": newSignature });
            query.exec((err) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve();
            });
        });
    };

    Facades.importHomePageDataFromMysql = function(data) {
        HomePageModel.collection.insertMany([data]);
    };
    module.exports = Facades;
})();