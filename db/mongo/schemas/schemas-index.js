(function () {
   const AutoIncrement = require('./auto-increment-schema');
   const HomePageSchema = require('./schema-homepage');
   module.exports = {AutoIncrement, HomePageSchema};
})();