'use strict';
(function () {
const mongoose = require('mongoose');
const Float = require('mongoose-float').loadType(mongoose);
const Schema = mongoose.Schema;
const options = {
    timestamps: true,
    collection: 'E_Pages'
};
const metadata = {
    key: {type: String, required: true, index: true, unique: true},
    title: String,
    description: String,
    blocks: [{  }]
};
module.exports = new Schema(metadata, options);
})();