const {debug, logger} = require('../../core/logger');
const {query} = require('./query');
const Facades = {};

Facades.getLocations = async function () {
    let cities = await query("select distinct `id`, `name` from `location_city` as `city`");
    let districts = await query("select distinct `id`, `name`, `city_id` from `location_district` as `district`");
    let wards = await query("select distinct `id`, `name`, `district_id` from `location_ward` as `ward`");
    if (cities && districts && wards) {
        locations = {cities: {}, districts: {}, wards: {}};

        cities.forEach(row => {
            const id = row['city']['id'];
            locations.cities[id] = {name: row['city']['name'], id: id};
        });

        districts.forEach(row => {
            const id = row['district']['id'];
            locations.districts[id] = {name: row['district']['name'], id: id, city_id: row['district']['city_id']};
        });

        wards.forEach(row => {
            const id = row['ward']['id'];
            locations.wards[id] = {name: row['ward']['name'], id: id, district_id: row['ward']['district_id']};
        });

        locations.getCity = function (id) {
            if (id) {
                return locations.cities[id.toString()];
            }

            return null;
        };

        locations.getDistrict = function (id) {
            if (id) {
                return locations.districts[id.toString()];
            }

            return null;
        };

        locations.getWard = function (id) {
            if (id) {
                return locations.wards[id.toString()];
            }

            return null;
        };

        return locations;
    }

    throw new Error("locations not found");
};

Facades.getPageByKey = async function (key) {
    let result = await query("select * from `pages_v3`  as `pages` where `key` = ?;", [key]);
    if(result && result.length > 0) {
        return result[0]['pages'];
    }
    throw new Error("page not found");
};

Facades.getPageBlockByPageId = async function (pageId) {
    let queryResult = await query("select * from `pages_blocks_v3`  as `pages_blocks` where `page_id` = ? AND `active` = 1;", [pageId]);
    let result = [];
    if(queryResult) {
        for (let i = 0; i < queryResult.length; i++) {
            if(queryResult[i]['pages_blocks']) {
                result.push(queryResult[i]['pages_blocks']);
            }
        }
    }
    return result;
};

Facades.getPageBlockProductByBlockId = async function (blockId) {
    let queryResult = await query("select * from `pages_blocks_products_v3` as `pages_blocks_products` where `block_id` = ?;", [blockId]);
    let result = [];
    if(queryResult) {
        for (let i = 0; i < queryResult.length; i++) {
            if(queryResult[i]['pages_blocks_products']) {
                result.push(queryResult[i]['pages_blocks_products']);
            }
        }
    }
    return result;
};

Facades.listCategory = async function () {
    let queryResult = await query("select * from `product_categories_v3` as `product_categories` order by `parent_id` ASC;");
    let result = [];
    if(queryResult) {
        for (let i = 0; i < queryResult.length; i++) {
            if(queryResult[i]['product_categories']) {
                result.push(queryResult[i]['product_categories']);
            }
        }
    }
    return result;
};
module.exports = Facades;