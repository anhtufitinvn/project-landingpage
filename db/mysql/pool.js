(function () {
    const mysql = require('mysql');
    const dbconfig = require('../../server/configuration').MYSQL;
    const SLAVE = dbconfig.SLAVE;
    const MASTER = dbconfig.MASTER;
    const PoolSlave = mysql.createPool({
        connectionLimit : SLAVE['limit'],
        host            : SLAVE['host'],
        user            : SLAVE['user'],
        password        : SLAVE['pwd'],
        database        : SLAVE['name'],
        debug           : false
    });

    const PoolMaster = mysql.createPool({
        connectionLimit : MASTER['limit'],
        host            : MASTER['host'],
        user            : MASTER['user'],
        password        : MASTER['pwd'],
        database        : MASTER['name'],
        debug           : false
    });

    module.exports = {PoolSlave, PoolMaster};
})();