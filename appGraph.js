var Express = require('express');
var Cors = require('cors');
var CookieParser = require('cookie-parser');
var {Logger} = require('./core/logger');
var graphql= require('express-graphql');
var {GraphResolver, GraphSchemas} = require('./graphs/graph');


/** using compression **/
var compression = require('compression');

var app = Express();

//// setup compression
app.use(compression());

//// setup CORS
app.use(Cors({credentials: true}));

app.use(require("morgan")("combined", {
    "stream": Logger.stream
}));

app.use(CookieParser());
/** setup graphql **/
app.use('/graph/v1', graphql(async (request, response, graphQLParams) => ({
        schema: GraphSchemas,
        context: {},
        rootValue: GraphResolver,
        graphiql: true,
        customFormatErrorFn: FormatErrorFn
    }))
);

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.json({code: -1, message: err.message, errors : []});
});

function errorConfig(error) {
    let ErrorMessage = {
        message: error.message,
        locations: error.locations,
        stack: error.stack ? error.stack.split('\n') : [],
        path: error.path
    };

    if(app.get('env') !== 'development') {
        ErrorMessage.stack = [];
    }

    return ErrorMessage;
}



const FormatErrorFn = error => (errorConfig(error));

module.exports = app;
