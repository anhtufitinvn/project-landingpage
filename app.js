var CreateError = require('http-errors');
var Express = require('express');
var Cors = require('cors');
var Path = require('path');
var CookieParser = require('cookie-parser');
var Providers = require('./server/providers');
var {Logger} = require('./core/logger');
var IndexRouter = require('./routes/index');
var BackendRouter = require('./routes/backend');

/** using compression **/
var compression = require('compression', (req, res) => {
    if (req.headers['x-no-compression']) {
        /** don't compress responses with this request header **/
        return false
    }

    /** fallback to standard filter function **/
    return compression.filter(req, res)
});

var app = Express();

//// setup compression
app.use(compression());

//// setup CORS
app.use(Cors({
    credentials: true
}));

//// views engine setup
app.set('views', Path.join(__dirname, 'mvvm', 'templates'));
app.set('view engine', 'twig');

app.use(require("morgan")("combined", {
    "stream": Logger.stream
}));

/** setup graph-sql mixing with res api and html5 **/
function graphErrorConfig(error) {
    let ErrorMessage = {
        message: error.message,
        locations: error.locations,
        stack: error.stack ? error.stack.split('\n') : [],
        path: error.path
    };

    if (app.get('env') !== 'development') {
        ErrorMessage.stack = [];
    }

    return ErrorMessage;
}

const graphql = require('express-graphql');
const GraphFormatErrorFn = error => (graphErrorConfig(error));
const {GraphResolver, GraphSchemas} = require('./graphs/graph');
app.use('/graph/v1', graphql(async (request, response, graphQLParams) => ({
        schema: GraphSchemas,
        context: {},
        rootValue: GraphResolver,
        graphiql: true,
        customFormatErrorFn: GraphFormatErrorFn
    }))
);

/** end graphs **/

/** setup static serve **/
app.use(Express.static(Path.join(__dirname, 'public')));
app.use('/static', Express.static(Path.join(__dirname, 'public')));

/** mvc index setup **/
app.use('/', CookieParser());
app.use('/', Express.json());
app.use('/', Express.urlencoded({extended: false}));
app.use('/', IndexRouter);

/** backend setup **/
app.use('/backend/v1', Express.json());
app.use('/backend/v1', Express.urlencoded({extended: false}));
app.use('/backend/v1', BackendRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(CreateError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    if (req.app.get('env') === 'development') {
        if (res.locals.isAPI || (req.originalUrl && req.originalUrl.startsWith('/api'))) {
            res.json({code: -1, message: err.message, stack: err.stack});
        } else {
            // render the error page
            res.status(err.status || 500);
            res.render('404', {base_url: BASE_URL});
        }

    } else {
        if (res.locals.isAPI || (req.originalUrl && req.originalUrl.startsWith('/api'))) {
            res.json({code: -1, message: err.message || "Something went wrong"});
        } else {
            // render the error page
            res.status(err.status || 500);
            res.render('404', {base_url: BASE_URL});
        }
    }
});

module.exports = app;
