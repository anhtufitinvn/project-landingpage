/**
 * Visit https://github.com/validatorjs/validator.js#validators to get more information.
 * isEmpty | isAscii | isBoolean | isFloat(num, {locale : 'en-US', min :0.0, max : 1.0 })
 * isEmail | isLatLong | isLength({min:0, max: undefined}) | isInt(x, { min: 10, max: 99 })
 * in["query"], in["body"], in["cookie"], in["headers"], in["params"]
 * sanitize functions :
 * toInt, toFloat, toBoolean, trim
 * blacklist() => blacklist(input, '\\[\\]')
 * escape(input)
 * normalizeEmail(input) call after isEmail()
 * sanitize(fields), sanitizeBody, sanitizeParam, sanitizeQuery, sanitizeCookie, sanitizeQuery
 * sanitizeParam('id').customSanitizer(value => {return ObjectId(value);})
 */
const rules = {
    testRule1: {
        string: {
            in: ["param"],
            errorMessage: 'string is not null or empty',
            not : true,
            isEmpty : true
        }
    },
    testRule2: {
        number: {
            in: ["param"],
            errorMessage: 'number is an integer',
            isInt: true,
            toInt : true
        }
    },
    testRule3: {
        uuid: {
            in: ["uuid"],
            errorMessage: 'uuid is not null or empty',
            customSanitizer : {
                options : (value, { req, location, path }) => {
                    return `your-uuid-${value}`;
                }
            }
        }
    }
};

rules.pageNum = {
    page: {
        in: ["query"],
        errorMessage: 'require page and page is an integer',
        isInt: true,
        toInt : true,
        optional: { options: { nullable: true } }
    }
};

rules.layoutId = {
    layout_id: {
        in: ["params"],
        errorMessage: 'require layout_id and layout_id is an integer',
        isInt: true,
        toInt: true
    }
};

rules.projectId = {
    project_id: {
        in: ["params"],
        errorMessage: 'require project_id and project_id is an integer',
        isInt: true,
        toInt: true
    }
};

rules.name = {
    name: {
        in: ["query"],
        errorMessage: 'require name and name is string',
        not : true,
        isEmpty : true
    }
};

rules.signature = {
    signature: {
        in: ["params"],
        errorMessage: 'require signature and signature is string',
        not : true,
        isEmpty : true
    }
};


rules.projects = {
    page: {
        in: ["query"],
        errorMessage: 'require page and page is an integer',
        isInt: true,
        toInt : true,
        optional: { options: { nullable: true } }
    },
    project_id: {
        in: ["query"],
        errorMessage: 'require project_id and project_id is an integer',
        isInt: true,
        toInt : true,
        optional: { options: { nullable: true } }
    }
};


rules.findEmail = {
    email: {
        in: ["query"],
        errorMessage: 'require email and email is only letters (a-zA-Z)',
        customSanitizer: {
            options: (value, { req, location, path }) => {
                let re = /[A-Za-z0-9\-_\.@\*]+/g;
                if(value === null || value === undefined || value === '') {
                    return "";
                }

                let m = value.match(re);
                if(m && m.length > 0) {
                    let t = "";
                    m.forEach(e => {
                        t = t+e;
                    });

                    return t;
                }

                return "";
            }
        }
    }
};

rules.email = {
    email: {
        in: ["query", "params"],
        errorMessage: 'require email',
        isEmail: true,
        normalizeEmail : true
    }
};
require('./partials/projects-rules')(rules);
module.exports = rules;