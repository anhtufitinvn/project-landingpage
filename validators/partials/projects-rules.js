(function () {
    const deleteProject = {
        project_id: {
            in: ["params"],
            errorMessage: 'require project_id : string',
            not: true,
            isEmpty: true
        }
    };

    const updateProject = {
        project_id: {
            in: ["params"],
            errorMessage: 'require project_id : string',
            not: true,
            isEmpty: true
        },
        project_name : {
            in : ['body'],
            errorMessage: 'require project_name : string',
            not: true,
            isEmpty: true
        },
        status : {
            in : ['body'],
            errorMessage: 'require status : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        },
        slug : {
            in : ['body'],
            errorMessage: 'require slug : string',
            customSanitizer : {
                options: (value) => {
                    return value ? value.toLowerCase() : value;
                }
            }
        }
    };

    module.exports = function (rules) {
        rules.deleteProject = deleteProject;
        rules.updateProject = updateProject;
    }
})();