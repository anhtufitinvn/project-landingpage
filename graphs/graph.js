const GraphSchemas = require('./schemas');
const GraphResolver = require('./resolver');
module.exports = {GraphSchemas, GraphResolver};