const {buildSchema} = require('graphql');
// Construct a schema, using GraphQL schema language
const typedef = `
    enum TypeOfPromotion {
       CODE,
       PAYMENT,
       OTHER
    },
    type Product {
        id : String!,
        name : String!
        price : Int!,
        promotions : [Promotion]
    },
    type PromoCondition {
        orderMinValue : Int!
        orderIsCombo : Boolean!
    },
    type Promotion {
        id : Int!,
        code : String!
        discountValue : Int!,
        discountPercent : Float!,
        discountMax : Int!
        conditions : [PromoCondition!]!,
        type : TypeOfPromotion,
        startTime : String!,
        endTime : String!
    },
    type Query {
        products : [Product],
        productsWithPromotion(type : TypeOfPromotion) : [Product]
    }
    
`;

module.exports = buildSchema(typedef);