(function () {
    const Products = require('../server/mockups/graphs-mockup').products;
    const GraphResolver = {
        products: () => {
            return Products;
        }
    };

    GraphResolver.productsWithPromotion = ({type}) => {
        return Products.filter((e) => {
            const promotion = e['promotions'];
            if (promotion && promotion.length > 0) {
                for (let i = promotion.length; --i >= 0;) {
                    if (promotion[i].type === type) {
                        return true;
                    }
                }
            }
            return false;
        })
    };

    module.exports = GraphResolver;
})();