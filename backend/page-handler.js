'use strict';

//// declare dependencies
let Providers = require('../server/providers');

let Handler = require('../core/handler');

/** init Handler **/

class PageHandler extends Handler {
    constructor() {
        super();
        this.name = "PageHandler";
    }

    async home(req, res, next) {
        this.Benchmark.start("getHomeData");
        let data = await Providers.DataAdapterService.getHomeData();
        this.Benchmark.end("getHomeData");
        res.json(this.makeReturnMessage(data));
    };

}

/** export Handler as singleton **/
module.exports = new PageHandler();
