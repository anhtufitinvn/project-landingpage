(function () {
    /**
     * inject all handlers as singletons
     */
    module.exports  = {
        MockupHandler : require('./mockups-handler'),
        ProductHandler : require('./products-handler'),
        PageHandler : require('./page-handler'),
        CategoryHandler : require('./category-handler'),
    }
})();