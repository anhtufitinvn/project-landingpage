(function () {
    /** load dependencies of this services **/
    let MapReduce = require('../map-reduce/map');
    let Utils = require('../core/utils');
    let Providers = require('../server/providers');
    let Handler = require('../core/handler');

    /** init Handler **/

    class MockupHandler extends Handler {
        constructor() {
            super();
            this.name = "MockupHandler";
        }

        async dump(req, res, next) {
            let collection = [
                {merchant_id: "JYSK", project_name: "JYSK-Product-1", price: 10, sku: "JYSK-1", qty : 2},
                {merchant_id: "JYSK", project_name: "JYSK-Product-2", price: 10, sku: "JYSK-2", qty : 1},
                {merchant_id: "JYSK", project_name: "JYSK-Product-3", price: 10, sku: "JYSK-3", qty : 1},
                {merchant_id: "FITIN", project_name: "FITIN-Product-2021", price: 20, sku: "FITIN-1", qty : 10},
                {merchant_id: "FITIN", project_name: "FITIN-Product-2022", price: 20, sku: "FITIN-2", qty : 12}
            ];
            let mapReduce = new MapReduce(collection,
                function (product, emit) {
                    emit(product.merchant_id, {
                        qty : product.qty,
                        price :product.price * product.qty
                    });
                },
                function (merchant_id, products) {
                    return {
                        total_price: Utils.sumBy(products, 'price'),
                        total_product : Utils.sumBy(products, 'qty')
                    } ;
                });

            let rs = await mapReduce.execute();

            // res.json(this.makeReturnMessage(Providers.MockupService.dump()));
            res.json(this.makeReturnMessage(rs));
        };

        async testEmail(req, res, next) {
            let email = req.params['email'];
            res.json(this.makeReturnMessage({your_email: email}));
        };

        async homes(req, res, next) {
            res.json(this.makeReturnMessage(Providers.MockupService.getHomes()));
        };
    }

    /** export Handler as singleton **/
    module.exports = new MockupHandler();
})();