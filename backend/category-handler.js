'use strict';

//// declare dependencies
const CategoryService = require('../services/singleton/category-services');

let Handler = require('../core/handler');

/** declare Handler **/

class CategoryHandler extends Handler {
    constructor() {
        super();
        this.name = "CategoryHandler";
    }

    async getTree(req, res, next) {
        this.Benchmark.start("CategoryHandler.getTree");
        // let data = await Providers.DataAdapterService.getCategoryTree();
        let data = CategoryService.getCategoryTree();
        this.Benchmark.end("CategoryHandler.getTree");
        res.json(this.makeReturnMessage(data));
    };

}

/** export Handler as singleton **/
module.exports = new CategoryHandler();
