(function () {
    /** load dependencies of this services **/

    let Providers = require('../server/providers');
    let Handler = require('../core/handler');
    let UrlService = Providers.UrlService;

    /** init Handler **/

    class ProductHandler extends Handler {
        constructor() {
            super();
            this.name = "ProductHandler";
        }

        async testWithEmail(req, res, next) {
            res.json(this.makeReturnMessage({
                handler : this.name,
                email : req.query["email"],
                avatar_url : UrlService.getStaticCDN()
            }));
        };

        async test(req, res, next) {
            res.json(this.makeReturnMessage({
                handler : this.name,
                avatar_url : UrlService.getStaticCDN()
            }));
        };
    }

    /** export Handler as singleton **/
    module.exports = new ProductHandler();
})();