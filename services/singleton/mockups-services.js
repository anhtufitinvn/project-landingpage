(function () {
    /** load dependencies of this services **/
    let ProductDetail = require('../../server/mockups/product-detail-mockup');
    let Guides = require('../../server/mockups/guides-mockup');
    let Homes = require('../../server/mockups/home-mockup');
    let CategoryDetail = require('../../server/mockups/category-detail-mockup');

    /** init service **/
    let Service = Object.create(require('../base-service')());
    Service.print(__filename);

    /** declare service variables **/

    let Logger = Service.Logger;
    let Debug = Service.Debug;

    /** Declare services functions **/

    Service.dump = function () {
        return ProductDetail;
    };

    Service.guides = function() {
        return Guides;
    };

    Service.getHomes = function () {
        return Homes;
    };

    Service.getProductDetail = function () {
        return ProductDetail;
    };

    Service.getCategoryDetail = function () {
        return CategoryDetail;
    };

    /** export service as singleton **/
    module.exports = Service;
})();