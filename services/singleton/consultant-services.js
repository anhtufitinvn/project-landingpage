'use strict';

/** load dependencies of this services **/
let Promise = require('bluebird');
let Request = require('request');

/** init service **/

let Service = Object.create(require('../base-service')());
Service.print(__filename);

/** declare service variables **/
let Logger = Service.Logger;
let Debug = Service.Debug;
let UrlService = require('./url-services');
/** Declare services functions **/
Service.getProjectById = function (projectId) {
    return new Promise((resolve, reject) => {
        const options = {
            headers: {
                "Content-Type": "application/json"
            },
            json: true,
            method: 'GET',
            url: UrlService.resolveProjectById(projectId)
        };

        Debug.console("call ", options.url);
        Request.get(options, (err, res, body) => {
            if (err) {
                Logger.error(err);
                reject(err);
            } else {
                if (body.code === -1 || body.data === undefined) {
                    reject(new Error(body.message || "invalid token"));
                } else {
                    if (body['data']) {
                        resolve(body['data']);
                    } else {
                        reject(new Error("product not found"));
                    }
                }
            }
        });
    });
};
Service.getRoomCategory = function () {
    return new Promise((resolve, reject) => {
        const options = {
            headers: {
                "Content-Type": "application/json"
            },
            json: true,
            method: 'GET',
            url: UrlService.resolveRoomCategory()
        };

        Debug.console("call ", options.url);
        Request.get(options, (err, res, body) => {
            if (err) {
                Logger.error(err);
                reject(err);
            } else {
                if (body.code === -1 || body.data === undefined) {
                    reject(new Error(body.message || "invalid token"));
                } else {
                    if (body['data']) {
                        resolve(body['data']);
                    } else {
                        reject(new Error("product not found"));
                    }
                }
            }
        });
    });
};
Service.getLayoutRenovationByRoomType = function (roomType) {
    return new Promise((resolve, reject) => {
        const options = {
            headers: {
                "Content-Type": "application/json"
            },
            json: true,
            method: 'GET',
            url: UrlService.resolveLayoutRenovationByRoomType(roomType)
        };

        Debug.console("call ", options.url);
        Request.get(options, (err, res, body) => {
            if (err) {
                Logger.error(err);
                reject(err);
            } else {
                if (body.code === -1 || body.data === undefined) {
                    reject(new Error(body.message || "invalid token"));
                } else {
                    if (body['data']) {
                        resolve(body['data']);
                    } else {
                        reject(new Error("product not found"));
                    }
                }
            }
        });
    });
};
/** export service as singleton **/
module.exports = Service;

