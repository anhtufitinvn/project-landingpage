(function () {
    /** load dependencies of this services **/
    let Path = require('path');
    let Moment = require('moment');

    /** init service **/
    let Service = Object.create(require('../base-service')());
    Service.print(__filename);

    /** declare service variables **/
    let Logger = Service.Logger;
    let Debug = Service.Debug;
    let URLS = Configuration.URLS;
    let ROOT_PATH = Configuration.STORAGE["root"];
    let STATIC_PATH = Configuration.STORAGE["static"];
    let PANORAMA_PATH = Configuration.STORAGE["panorama"];


    /** Declare services functions **/

    Service.getConsultantCDN = function () {
        return URLS["cdn-consultant"] || null;
    };

    Service.getStaticCDN = function () {
        return URLS["cdn"] || null;
    };

    Service.getEcomCDN = function () {
        return `${URLS["cdn"]}/${URLS["cms_ecom"]}`;
    };


    Service.isWindow = function () {
        return process.platform === "win32";
    };

    Service.getNameWithoutExt = function (file) {
        let name = Service.basename(file);
        return name.replace(/\.[^/.]+$/, "");
    };

    Service.getFileExt = function (file) {
        let name = this.basename(file).toLowerCase();
        let i = name.lastIndexOf(".");
        if (i >= 0) {
            return name.substring(i, name.length);
        }

        return "";
    };

    Service.replaceAllSpace = function (string) {
        if (string) {
            return string.replace(/ /g, "");
        }

        return null;
    };

    Service.replaceAllSpaceWithUnderscore = function(string) {
        if (string) {
            return string.replace(/ /g, "_");
        }

        return null;
    };

    Service.basename = function (file) {
        if (file === undefined || file === null) {
            return null;
        }

        let f = Path.normalize(file);
        if (process.platform === "win32") {
            return Path.win32.basename(f);
        }

        return Path.posix.basename(f);
    };

    Service.dirname = function (file) {
        if (file === undefined || file === null) {
            return null;
        }

        let f = Path.normalize(file);
        return Path.dirname(f);
    };

    Service.join = function (p1, p2) {
        return Path.join(p1, p2);
    };

    Service.resolveProjectImageUrl = function (imageURL) {
        if (imageURL) {
            if (imageURL.startsWith("http")) {
                return imageURL;
            }

            return `${Service.getEcomCDN()}/${imageURL}`
        }

        return null;
    };

    Service.resolveEcomCDNUrl = function (imageURL) {
        if (imageURL) {
            if (imageURL.startsWith("http")) {
                return imageURL;
            }

            return `${Service.getEcomCDN()}/${imageURL}`
        }

        return null;
    };

    Service.resolvePanoramaImageUrl = function (filePath) {
        let p = filePath.substring(PANORAMA_PATH.length);
        if (Service.isWindow()) {
            p = p.replace(/\\/g, '/');
        }

        if (p.startsWith("/")) {
            return `${URLS["panorama_image_url"]}${p}`;
        } else {
            return `${URLS["panorama_image_url"]}/${p}`;
        }
    };

    Service.resolveCartUrl = function (signature) {
        if (signature) {
            return `${URLS['cart_url']}?signature=${signature}&refer=${URLS['domain']}`
        }
        return null;
    };

    Service.resolveStaticFilePathUrl = function (filePath) {
        let p = filePath.substring(STATIC_PATH.length);
        if (Service.isWindow()) {
            p = p.replace(/\\/g, '/');
        }

        return `${Service.getStaticCDN()}${p}`;
    };

    /** Convert a absolute path to relative path **/
    Service.resolveStaticFileRelativePath = function (filePath) {
        let p = filePath.substring(STATIC_PATH.length);
        if (this.isWindow()) {
            p = p.replace(/\\/g, '/');
        }

        return p;
    };

    /** Convert a relative path to static url **/
    Service.resolveStaticRelativePathUrl = function (relativePath) {
        let p = relativePath;
        if (this.isWindow()) {
            p = p.replace(/\\/g, '/');
        }

        if (p.startsWith("/")) {
            return `${Service.getStaticCDN()}${p}`;
        }

        return `${Service.getStaticCDN()}/${p}`;
    };

    Service.resolvePanoramaUrl = function (ticket) {
        return `${URLS['panorama_view_url']}/${ticket}`;
    };


    Service.resolveQuotationUrl = function (signature) {
        return `${URLS['quotation_url']}/quotation/${signature}`;
    };

    Service.resolveLayoutQuotationUrl = function (layout_id) {
        return `${URLS['quotation_url']}/quotation/layouts/${layout_id}`;
    };

    Service.resolvePDFQuotationUrl = function (signature) {
        let t = moment().unix();
        return `${Service.getConsultantCDN()}/qt/${signature}.pdf?t=${t}`;
    };

    Service.resolveProjectById = function (projectId) {
        return `${URLS['quotation_url']}/api/v2/projects/${projectId}`;
    };

    Service.resolveRoomCategory = function () {
        return `${URLS['quotation_url']}/api/v2/resources/room-categories`;
    };

    Service.resolveLayoutRenovationByRoomType = function (roomType) {
        return `${URLS['quotation_url']}/api/v2/renovation/layouts?page=1&room_type=${roomType}`;
    };

    /** export service as singleton **/
    module.exports = Service;
})();