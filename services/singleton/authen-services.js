(function () {
    /** load dependencies of this services **/
    let Promise = require('bluebird');
    let Request = require('request');

    /** init service **/

    let Service = Object.create(require('../base-service')());
    Service.print(__filename);

    /** declare service variables **/

    let FITIN_ID_URL = Configuration.URLS["fitin_id_url"];
    let Logger = Service.Logger;
    let Debug = Service.Debug;

    /** Declare services functions **/

    Service.verifyToken = async function(accessToken) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    'X-App-Id': "object-editor",
                    'X-App-Secret': "39f1590ebd5dd409df676f2f3f0ea8552c90fadc"
                },
                body: {
                    token: accessToken
                },
                json: true,
                method: 'POST',
                url: `${FITIN_ID_URL}/user/credential/token/verify`
            };

            Debug.console("call ", options.url);
            Request.post(options, (err, res, body) => {
                if (err) {
                    Logger.error(err);
                    reject(err);
                } else {
                    if (body.code === -1 || body.data === undefined) {
                        reject({message: body.message || "invalid token"});
                    } else {
                        if (body['data']) {
                            const data = body.data;
                            resolve({
                                uid: data.uid || 0,
                                email: data.email,
                                token: data.token,
                                expire: data.expire
                            });
                        } else {
                            reject({message: "invalid token"});
                        }
                    }
                }
            });
        });
    };

    /** export service as singleton **/
    module.exports = Service;
})();