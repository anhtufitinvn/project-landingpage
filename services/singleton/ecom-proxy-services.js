(function () {
    'use strict';

    /** load dependencies of this services **/
    let Promise = require('bluebird');
    let Request = require('request');

    /** init service **/

    let Service = Object.create(require('../base-service')());
    Service.print(__filename);

    /** declare service variables **/
    const FITIN_API_ECOM_URL = `${Configuration.URLS["fitin_api_ecom_url"]}/v1`;
    const FITIN_APP_PLATFORM = `${Configuration.FITIN["app_platform"]}`;
    const FITIN_APP_VERSION = `${Configuration.FITIN["app_version"]}`;
    let Logger = Service.Logger;
    let Debug = Service.Debug;

    /** Declare services functions **/
    Service.getProductDetail = function (productId) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json"
                },
                json: true,
                method: 'GET',
                url: `${FITIN_API_ECOM_URL}/products-v2/${productId}`
            };

            Debug.console("call ", options.url);
            Request.get(options, (err, res, body) => {
                if (err) {
                    Logger.error(err);
                    reject(err);
                } else {
                    if (body.code === -1 || body.data === undefined) {
                        reject(new Error(body.message || "invalid token"));
                    } else {
                        if (body['data']) {
                            resolve(body['data']);
                        } else {
                            reject(new Error("product not found"));
                        }
                    }
                }
            });
        });
    };
    Service.getListProductCardView = function (productIds) {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json"
                },
                json: true,
                method: 'GET',
                url: `${FITIN_API_ECOM_URL}/products/filters?id=${productIds}`
            };

            Debug.console("call ", options.url);
            Request.get(options, (err, res, body) => {
                if (err) {
                    Logger.error(err);
                    reject(err);
                } else {
                    if (body.code === -1 || body.data === undefined) {
                        reject(new Error(body.message || "invalid token"));
                    } else {
                        if (body['data']) {
                            resolve(body['data'].list);
                        } else {
                            reject(new Error("product not found"));
                        }
                    }
                }
            });
        });
    };
    Service.getHomeData = function () {
        return new Promise((resolve, reject) => {
            const options = {
                headers: {
                    "Content-Type": "application/json",
                    "Fitin-App-Platform": FITIN_APP_PLATFORM,
                    "Fitin-App-Version": FITIN_APP_VERSION,
                },
                json: true,
                method: 'GET',
                url: `${FITIN_API_ECOM_URL}/page/home`
            };

            Debug.console("call ", options.url);
            Request.get(options, (err, res, body) => {
                if (err) {
                    Logger.error(err);
                    reject(err);
                } else {
                    if (body.code === -1 || body.data === undefined) {
                        reject(new Error(body.message || "invalid data"));
                    } else {
                        if (body['data']) {
                            resolve(body['data']);
                        } else {
                            reject(new Error("page not found"));
                        }
                    }
                }
            });
        });
    };
    /** export service as singleton **/
    module.exports = Service;

})();