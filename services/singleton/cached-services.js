(function () {
    let Service = Object.create(require('../base-service')());
    Service.print(__filename);
    const NodeCache = require( "node-cache" );
    const CommonCache = new NodeCache();
    Service.set = function(key, value, ttl) {
        CommonCache.set(key, value, ttl || 600);
    };

    Service.get = function(key) {
        let value = CommonCache.get(key);
        if (value === undefined ){
            return null;
        }

        return value;
    };

    Service.flushAll = function() {
        CommonCache.flushAll();
    };

    module.exports = Service;
})();