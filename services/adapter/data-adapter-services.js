(function () {
    /** load dependencies of this services **/
    let Utils = require('../../core/utils');
    let ProxyService = require('../singleton/ecom-proxy-services');
    const MysqlFacade = require('../../db/mysql/mysql-facades');
    const TransformModule = require('../../transform');
    // const MongoFacade = require('../../db/mongo/mongo-facades');

    /** init service **/
    let Service = Object.create(require('../base-service')());
    Service.print(__filename);

    /** declare service variables **/

    let Logger = Service.Logger;
    let Debug = Service.Debug;
    let Benchmark = Service.Benchmark;

    /** Declare services functions **/

    Service.getProductDetail = async function (productId) {
        if (!Utils.isNumber(productId)) {
            throw new Error("productId is invalid");
        }

        let data = await ProxyService.getProductDetail(productId);
        if (data) {
            let sdata = JSON.stringify(data);
            let compressed = await Utils.compressWithBase64(sdata);
            //// TODO add compressed data to redis for caching
            Debug.console("data  :", sdata.length);
            Debug.console("compressed data :", compressed.length);
        }

        return data;
    };

    Service.getListProductCardView = async function (productIds) {
        Benchmark.start("getListProductCardView");
        let data = await ProxyService.getListProductCardView(productIds);
        if (data) {
            let sdata = JSON.stringify(data);
            let compressed = await Utils.compressWithBase64(sdata);
            //// TODO add compressed data to redis for caching
            Debug.console("data  :", sdata.length);
            Debug.console("compressed data :", compressed.length);
        }

        Benchmark.end("getListProductCardView");
        return data;
    };

    Service.getHomeData = async function () {
        const keyHome = 'home';
        let result = {};
        let pageHome = await MysqlFacade.getPageByKey(keyHome);
        //
        result.id = pageHome.id;
        result.key = keyHome;
        result.title = pageHome.title;
        result.description = pageHome.description;
        result.content = pageHome.content;
        //
        let blocks = [];
        //
        result.blocks = blocks;
        //
        let listPageBlock = await MysqlFacade.getPageBlockByPageId(pageHome.id);
        for (let i = 0; i < listPageBlock.length; i++) {
            const pageBlock = listPageBlock[i];
            let arrBlockDetail = TransformModule.PageBlock.headerData(pageBlock);
            let arrBlockDetailData = [];
            if (pageBlock.block_type === 'banner') {
                if (pageBlock.data_json) {
                    // foreach, group by position
                    const dataJson = JSON.parse(pageBlock.data_json);
                    let arrByPosition = {};
                    dataJson.forEach(item => {
                        if (item.position) {
                            item.position = item.position.replace("position_", "");
                        }
                        let position = item.position || 1;
                        if (!arrByPosition[position]) {
                            arrByPosition[position] = [];
                        }
                        arrByPosition[position].push(item);
                    });
                    arrBlockDetailData = arrByPosition;
                }
            } else if (pageBlock.block_type === 'keyword'
                || pageBlock.block_type === 'brand'
                || pageBlock.block_type === 'icons'
                || pageBlock.block_type === 'blogs'
                || pageBlock.block_type === 'projects'
                || pageBlock.block_type === 'category'
            ) {
                if (pageBlock.data_json) {
                    // foreach, group by position
                    let arrByPosition = [];
                    const dataJson = JSON.parse(pageBlock.data_json);
                    if(dataJson) {
                        dataJson.forEach(item => {
                            arrByPosition.push(item);
                        });
                    }
                    arrBlockDetailData = arrByPosition;
                }
            } else if (pageBlock.block_type === 'product') {
                // get product in block
                let listPageBlockProduct = await MysqlFacade.getPageBlockProductByBlockId(pageBlock.id);
                // insert data product-id
                let productIds = '';
                for (let i = 0; i < listPageBlockProduct.length; i++) {
                    let p = listPageBlockProduct[i];
                    let pid = p.product_id;
                    productIds += pid + ',';
                }
                let productDetail = await Service.getListProductCardView(productIds);
                arrBlockDetailData = productDetail;
            }
            arrBlockDetail['list_data'] = arrBlockDetailData;
            blocks[i] = arrBlockDetail;
        }
// MongoFacade.importHomePageDataFromMysql(result);
        return result;
    };

    Service.getCategoryTree = async function () {
        let listCategory = await MysqlFacade.listCategory();
        return TransformModule.Category.buildTree(listCategory);
    };
    /** export service as singleton **/
    module.exports = Service;
})();