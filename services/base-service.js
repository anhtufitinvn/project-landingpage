(function () {
    const {Logger, Debug, Benchmark} = require('../core/logger');
    const path = require('path');
    module.exports = function () {
        return {
            Debug: Debug,
            Logger: Logger,
            Benchmark : Benchmark,
            print : function (name) {
                let f = path.normalize(name);
                if (process.platform === "win32") {
                    name = path.win32.basename(f);
                } else {

                    name = path.posix.basename(f);
                }

                Logger.info(`[OK] load service [${name}]`);
            }
        }
    };
})();