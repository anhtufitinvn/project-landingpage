jQuery(document).ready(function ($) {
    if ($('.download-button button').length) {
        $('.download-button button').click(function (event) {
            event.preventDefault();
            $('.support-form').toggleClass('show');
        });
        $('.support-form .close-button').click(function () {
            $('.support-form').toggleClass('show');
        })
    }
    if ($('.form-support form, .support-form form').length) {
        $('.form-support form, .support-form form').validate();
        $('.form-support form, .support-form form').submit(function (event) {
            event.preventDefault();
            const $form = $($('.form-support form, .support-form form').get(0));
            const data = {
                name: $form.find('input[name="customer-name"]').val(),
                phone: $form.find('input[name="phone"]').val(),
                email: $form.find('input[name="email"]').val(),
                project: $form.find('input[name="project"]').val(),
                message: $form.find('select').val(),
                tower: $form.find('input[name="tower"]').val(),
                cookie: document.cookie,
                hubspotutk: Cookies.get("hubspotutk")
            };
            if ($form.find('input[name="layout_id"]').val()) {
                data.layout_id = $form.find('input[name="layout_id"]').val();
            }
            axios.post(ApiSubmitHubspot, data).then(function (response) {
                if (response.data.code === 0) {
                    swal({
                        icon: "success",
                        title: "Gửi thành công qua email",
                        text: "Vui lòng kiểm tra email của bạn để xem báo giá nhé!"
                    });
                    $form[0].reset();
                    $('.support-form').toggleClass('show');
                    try {
                        dataLayer.push({ event: "lead" });
                    } catch(error) {
                        console.log(error);
                    }
                }
            });
        })
    }

    if ($('.banner-video-wrapper .img-cover').length) {
        $('.banner-video-wrapper .img-cover').hide();
    }
    if ($('.project-detail-wp .slider-thumb').length) {
        $('.project-detail-wp .slider-thumb').slick({
            infinite: false,
            focusOnSelect: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            dots: false,
            autoplay: false,
            autoplaySpeed: 3000,
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 3
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 1
                  }
                }
            ]
        });
        $(".project-detail-wp .slider-thumb .item").click(function () {
            const $item = $(this);
            if ($item.hasClass('panorama')) {
                $('.big-img').hide();
                $('.big-iframe').show();
            } else {
                $('.big-iframe').hide();
                $('.big-img').show();
                $('.big-img').css('background-image', `url("${$item.data('url')}")`)
            }
        });
    }
    if ($('.apartment-style-slider-wrapper-1').length) {
        $('.apartment-style-slider-wrapper-1').slick({
            infinite: true,
            focusOnSelect: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: false,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 3
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 1
                  }
                }
            ]
        })
    }
    if ($('.apartment-style-slider-wrapper-2').length) {
        $('.apartment-style-slider-wrapper-2').slick({
            infinite: true,
            focusOnSelect: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: false,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 3
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 1
                  }
                }
            ]
        })
    }
    if ($('.apartment-style-slider-wrapper-3').length) {
        $('.apartment-style-slider-wrapper-3').slick({
            infinite: true,
            focusOnSelect: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: false,
            autoplay: true,
            autoplaySpeed: 3000,
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 3
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30px',
                    slidesToShow: 1
                  }
                }
            ]
        })
    }
    if ($('.steps-block .fit-step-slider').length) {
        const $sliderStep = $('.steps-block .fit-step-slider').slick({
            infinite: true,
            centerMode: true,
            focusOnSelect: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            dots: false,
        });
        $(".steps-block .fit-step-slider").on("afterChange", function (
            event,
            slick,
            direction
        ) {
            const active = slick.currentSlide + 1;
            $(".steps-block .step").each(function () {
                if (parseInt($(this).data('init')) <= active) {
                    $(this).addClass('active')
                } else {
                    $(this).removeClass('active')
                }
            });
        });
        $(".steps-block .step").click(function () {
            const index = parseInt($(this).data('init'));
            $sliderStep.slick('slickGoTo', index - 1);
        })
    }
})