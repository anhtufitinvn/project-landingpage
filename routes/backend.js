var Express = require('express');
var Router = Express.Router();
var {Logger, debug} = require('../core/logger');
var Handlers = require('../backend/modules');
var Validator = require('../validators/validator');
var {dispatch} = require('../core/dispatcher');

/** Mockups modules **/
Router.get('/mockups',
    dispatch(Handlers.MockupHandler, Handlers.MockupHandler.dump));

Router.get('/mockups/email/:email',
    Validator.validate('email'),
    dispatch(Handlers.MockupHandler, Handlers.MockupHandler.testEmail));

Router.get('/mockups/homes',
    dispatch(Handlers.MockupHandler, Handlers.MockupHandler.homes));


Router.get('/pages/home',
    dispatch(Handlers.PageHandler, Handlers.PageHandler.home));
Router.get('/category/tree',
    dispatch(Handlers.CategoryHandler, Handlers.CategoryHandler.getTree));

module.exports = Router;
