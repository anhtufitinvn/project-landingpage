var Express = require('express');
var Router = Express.Router();
var Controllers = require('../mvvm/modules');
var {resolve} = require('../core/dispatcher');

/* GET home page. */
Router.get('/', resolve(Controllers.Index, "indexAction"));
Router.get('/thiet-ke/can-ho-:number_bed_room-phong-ngu', resolve(Controllers.Index, "designAction"));
Router.get('/chi-tiet/*-:layout_id', resolve(Controllers.Index, "detailAction"));
// Renovation
Router.get('/:room_type/*-:layout_id', resolve(Controllers.Index, "detailRenovationAction"));
Router.get('/:room_type', resolve(Controllers.Index, "roomRenovationAction"));


Router.get('/samples', resolve(Controllers.Sample, 'sampleAction'));

Router.get('/guides', resolve(Controllers.Guides));

module.exports = Router;
