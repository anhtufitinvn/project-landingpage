const fs = require('fs');
const path = require('path');
const access_key = "9OAMUJHalcpJcR0CohSNTXDirqA";
const prefix = "profile-";
const VERSION = '2020.02.03-b1';
var configFile = 'local-config.json';
const NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV.toLowerCase() : "local";
if (NODE_ENV === 'production') {
    configFile = 'config.json';
} else if (NODE_ENV === 'development') {
    configFile = 'dev-config.json';
} else {
    configFile = 'local-config.json';
}

const configPath = path.resolve(path.dirname(__dirname), configFile);
if(!fs.existsSync(configPath)) {
    console.error(`${configFile} was not found`);
    process.exit(-1);
}

const jsConfig = JSON.parse(fs.readFileSync(configPath));
console.log({config: configFile});
if (jsConfig === null || jsConfig === undefined) {
    process.exit(-1);
} else {
    global.ACCESS_KEY = jsConfig['access_key'] || access_key;
    global.BASE_URL = jsConfig['base_url'] || "/";
    module.exports = {
        HTTPS : jsConfig['https'] || false,
        WORKER_ID: jsConfig['worker_id'] || null,
        PORT: jsConfig['port'] || 4916,
        PORT_GRAPHS : jsConfig['port_graphs'] || 4917,
        VERSION: jsConfig['version'] || VERSION,
        LOG_LEVEL: jsConfig['log_level'],
        REDIS: jsConfig['redis'] || {host: "localhost", port: 6379, prefix: prefix},
        ACCESS_KEY: jsConfig['access_key'] || access_key,
        RENDER_ACCESS_KEY: jsConfig['render_access_key'] || "APNpLHHSJYQ9uV4R",
        MAIL_ACCESS_KEY : jsConfig["mail_access_key"] || "VaX8pHGLBw",
        RABBITMQ: jsConfig['rabbitmq'] || "localhost",
        MYSQL: { SLAVE : jsConfig['mysql']['slave'], MASTER : jsConfig['mysql']['master']},
        MONGODB: jsConfig['mongodb'],
        URLS: jsConfig['urls'],
        STORAGE: jsConfig['storage'],
        HUBSPOT: jsConfig['hubspot'],
        GA_ID: jsConfig['ga_id'],
        GTM_ID: jsConfig['gtm_id'],
        GOOGLE_ID: jsConfig['google_id'],
        FACEBOOK_ID: jsConfig['facebook_id'],
        FITIN: jsConfig['fitin']
    }
}