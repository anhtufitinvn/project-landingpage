(function () {
    /** container is a private variable, it cannot access from out side of Provider object **/
    let container = {};

    let Providers = {
        setShared (name, instance) {
            if(name && instance) {
                let key = name.toLowerCase();
                container[key] = instance;
            }
        },
        get(name) {
            if(name) {
                return container[name.toLowerCase()] || null;
            }

            return null;
        }
    };

    /** binding function, can use 'this' inside of function  **/

    Providers.setShared = Providers.setShared.bind(Providers);
    Providers.get = Providers.get.bind(Providers);

    /** inject all singleton services **/

    Providers.AuthenService  = require('../services/singleton/authen-services');
    Providers.MockupService = require('../services/singleton/mockups-services');
    Providers.UrlService = require('../services/singleton/url-services');
    Providers.DataAdapterService = require('../services/adapter/data-adapter-services');
    Providers.CacheService = require('../services/singleton/cached-services');
    Providers.ConsultantService = require('../services/singleton/consultant-services');

    module.exports = Providers;
})();