(function () {
    class MapReduce {
        constructor(collection, mapFunction, reduceFunction) {
            this.collection = collection;
            this.reduceFunction = reduceFunction;
            this.mapFunction = mapFunction;
        }

        async execute() {
            if (this.collection && this.collection.length > 0) {
                const hasMapFunction = typeof this.mapFunction === 'function' || (this.mapFunction instanceof Function);
                const hasReduceFunction = typeof this.reduceFunction === 'function' || (this.reduceFunction instanceof Function);
                if (hasMapFunction && hasReduceFunction) {
                    let mapData = {};
                    let outputData = {};
                    let emit = function (key, value) {
                        if (key && value) {
                            if (mapData[key] === undefined || mapData[key] === null) {
                                mapData[key] = [];
                            }

                            mapData[key].push(value);
                        }
                    };

                    for (let d of this.collection) {
                        this.mapFunction(d, emit);
                    }

                    Object.entries(mapData).forEach(async (entry) => {
                        /***  key = entry[0], value = entry[1]; ***/
                        let rs = await this.reduceFunction(entry[0], entry[1]);

                        if (outputData[entry[0]] === undefined || outputData[entry[0]] === null) {
                            outputData[entry[0]] = [];
                        }

                        outputData[entry[0]] = rs
                    });

                    return outputData;
                }

            }

            return null;
        }
    }

    module.exports = MapReduce;
})();