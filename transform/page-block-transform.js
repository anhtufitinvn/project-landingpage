'use strict';
(function () {
    let T = {};

    T.headerData = function (block) {
        return  {
            id: block.id,
            title: block.title,
            block_type: block.block_type,
            template_name: block.template_name,
            configs: JSON.parse(block.configs)
        };
    };

    module.exports = T;
})();
