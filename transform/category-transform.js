'use strict';
(function () {
    let T = {};

    T.buildTree = function (list) {
        let tree = {};
        for (let i = 0, N = list.length; i < N; i++) {
            let iid = `_${list[i]['id']}`;
            if (!tree[iid]) {
                tree[iid] = [];
            }
            let item = list[i];
            let newItem = {
                id: item.id,
                name: item.name,
                slug: item.slug,
                url: item.url,
                parent_id: item.parent_id,
                image: item.image,
                product_count: item.product_count,
                children: []
            };
            tree[iid] = {...tree[iid], ...newItem};
            //
            let pid = 'root';
            if (item['parent_id']) {
                pid = `_${item['parent_id']}`;
            }
            if (!tree[pid]) {
                tree[pid] = {};
            }
            if (!tree[pid]['children']) {
                tree[pid]['children'] = [];
            }
            tree[pid]['children'].push(tree[iid]);
            if (pid === 'root') {
                if (!tree[pid]['product_count']) {
                    tree[pid]['product_count'] = 0;
                }
                tree[pid]['product_count'] += tree[iid]['product_count'];
            }
        }
        return tree['root'];
    };

    module.exports = T;
})();
