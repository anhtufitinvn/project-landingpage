const {createLogger, format, transports} = require('winston');
const {combine, timestamp, printf} = format;

const myFormat = printf(({level, message, timestamp}) => {
    return `${timestamp} ${level}: ${message}`;
});

const Logger = createLogger({
    level: 'info',
    format: combine(
        timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        myFormat
    ),
    defaultMeta: {service: 'fitin-ecom-v3'},
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log`
        // - Write all logs error (and below) to `error.log`.
        //
        new transports.File({filename: '../logs/fitin-ecom-v3/error.log', level: 'error'}),
        new transports.File({filename: '../logs/fitin-ecom-v3/combined.log'}),
    ],
    exitOnError: false
});

const Tracer = createLogger({
    level: 'info',
    format: combine(
        timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        myFormat
    ),
    defaultMeta: {service: 'fitin-ecom-v3'},
    transports: [
        new transports.File({filename: '../logs/fitin-ecom-v3/access.log', level: 'info'})
    ],
    exitOnError: false
});


Logger.stream = {
    write: function (message) {
        Tracer.info(message);
    }
};

if (process.env.NODE_ENV !== 'production') {
    Logger.add(new transports.Console({
        format: combine(
            timestamp({
                format: 'YYYY-MM-DD HH:mm:ss'
            }),
            myFormat
        )
    }));
}


let Debug = {
    console: function (message, args) {
        if (process.env.NODE_ENV !== 'production') {
            if (args) {
                console.log(message, args);
            } else {
                console.log(message);
            }
        }
    },
    error: function (message, args) {
        if (process.env.NODE_ENV !== 'production') {
            if (args) {
                console.error(message, args);
            } else {
                console.error(message);
            }
        }
    }
};

let Benchmark = {
    start : (label) => {
        if (process.env.NODE_ENV !== 'production') {
            console.time(`[Benchmark] [${label}]`);
        }
    },

    end : (label) => {
        if (process.env.NODE_ENV !== 'production') {
            console.timeEnd(`[Benchmark] [${label}]`);
        }
    }
};

module.exports = {logger : Logger, debug : Debug.console, Logger, Debug, Benchmark };