(function () {
    //// declare dependencies
    let Zlib = require('zlib');
    let Promise = require('bluebird');

    let Utils = {};

    Utils.isString = function (v) {
        return !!(v && typeof v === 'string');
    };

    Utils.isNumber = function (v) {
        return !!(v && typeof v === 'number');
    };

    Utils.isNullOrEmpty = function (v) {
        if (v === undefined || v === null) {
            return true;
        }

        return typeof v === 'string' && v.length === 0;

    };

    Utils.compressWithBase64 = function (data) {
        return new Promise((resolve, reject) => {
            Zlib.deflate(data, (error, compressedData) => {
                if (error) {
                    return reject(error);
                }

                return resolve(compressedData.toString('base64'));
            })
        });
    };

    Utils.uncompressWithBase64 = function (compressedData) {
        return new Promise((resolve, reject) => {
            Zlib.inflate(new Buffer(compressedData, 'base64'), (error, data) => {
                if (error) {
                    return reject(error);
                }

                return resolve(data);
            })
        });
    };

    Utils.sum = function(array) {
        if(array && array.length > 0) {
            return array.reduce((total, current) => {
                return total + current
            }, 0);
        }

        return 0;
    };

    Utils.sumBy = function(array, by) {
        if(array && array.length > 0) {
            return array.reduce((total, current) => {
                return total + current[by]
            }, 0);
        }

        return 0;
    };

    module.exports = Utils;
})();