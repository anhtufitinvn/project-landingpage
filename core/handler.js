(function () {
    /** load dependencies **/
    const {matchedData, checkSchema, validationResult} = require('express-validator');
    const RuleList = require('../validators/rules-schema');
    const Providers = require('../server/providers');
    const {Logger, Debug, Benchmark} = require('../core/logger');

    /** init Handler **/

    class Handler {
        constructor() {
            this.Providers = Providers;
            this.RuleSchemas = {};
            this.Logger = Logger;
            this.Debug = Debug;
            this.debug = Debug.console;
            this.Benchmark = Benchmark;
        }

        loadRules(rules) {
            if (rules && rules.length > 0) {
                rules.map(r => {
                    if (RuleList[r] && !this.RuleSchemas[r]) {
                        this.RuleSchemas[r] = checkSchema(RuleList[r]);
                    }
                })
            }
        };

        loadAllRules() {
            Object.keys(RuleList).forEach(name => {
                if(!name.startsWith("test")) {
                    if (RuleList[name] && !this.RuleSchemas[name]) {
                        this.Debug.console(`load rule[${name}]`);
                        this.RuleSchemas[name] = checkSchema(RuleList[name]);
                    }
                }
            });
        };

        errorHandler(req, res, next) {
            const errors = validationResult(req);
            if (errors.isEmpty()) {
                req.matchedData = matchedData(req);
                return next();
            }

            const extractedErrors = [];
            errors.array().map(err => extractedErrors.push({[err.param]: err.msg}));
            return res.status(200).json({
                code: -1,
                message: "invalid parameters",
                errors: extractedErrors,
            });
        };

        makeReturnMessage (data) {
            return {
                code: 0,
                message: "success",
                data: data
            };
        };

        makeReturnError (message, errorCode) {
            if (message instanceof Error) {
                return {
                    code: -1,
                    message: "got an exception",
                    errors: [
                        {
                            error: errorCode || -1,
                            message: message["message"] || "something went wrong"
                        }
                    ]
                };
            }

            return {
                code: -1,
                message: message,
                errors: [
                    {
                        error: errorCode || -1,
                        message: message
                    }
                ]
            };
        };

        validate(ruleName) {
            if (ruleName && this.RuleSchemas[ruleName]) {
                return [this.RuleSchemas[ruleName], this.errorHandler];
            }

            return [];
        };
    }

    module.exports = Handler;
})();