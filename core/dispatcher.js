(function () {
    const {debug} = require('../core/logger');

    function dispatch(handler, fn) {
        return async (req, res, next) => {
            try {
                res.locals.isAPI = true;
                if (handler) {
                    if (typeof fn === 'function' || fn instanceof Function) {
                        await fn.call(handler, req, res);
                    } else {
                        let action = handler[fn];
                        if (action && (typeof action === 'function' || action instanceof Function)) {
                            await action.call(handler, req, res);
                        } else {
                            next();
                        }
                    }
                } else {
                    next();
                }
            } catch (e) {
                debug(e);
                next(e);
            }
        }
    }

    function resolve(controller, action) {
        return async (req, res, next) => {
            try {
                if (controller && controller.handle) {
                    if(action === undefined || action === null) {
                        return await controller.handle.call(controller, req, res, 'indexAction');
                    }

                    if (typeof action === 'string' || action instanceof String) {
                        if(!action.endsWith('Action')) {
                            action = action + 'Action';
                        }

                        return await controller.handle.call(controller, req, res, action);
                    }

                    return next();
                }
                /** finally call next event **/
                next();
            } catch (e) {
                debug(e);
                next(e);
            }
        }
    }

    module.exports = {dispatch, resolve};
})();