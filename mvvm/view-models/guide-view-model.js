(function () {
    /** declare dependencies **/
    let Providers = require('../../server/providers');

    /** ViewMode is an abstract class  **/
    let ViewMode = require('./view-model');

    class GuideViewModel extends ViewMode {
        constructor() {
            super();
            //// set view template file : test/test-guides.twig
            this.ViewName = "test/test-guides";
            //// set html <title></title>
            this.setPageTitle("Guides");
        }

        async getContent(args) {
            return Providers.MockupService.guides();
        }
    }

    module.exports = GuideViewModel;
})();
