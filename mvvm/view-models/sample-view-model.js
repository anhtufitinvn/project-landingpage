(function () {
    /** ViewMode is an abstract class  **/
    let ViewMode = require('./view-model');

    class SampleViewModel extends ViewMode {
        constructor() {
            super();
            //// set view template file : test/test-sample.twig
            this.ViewName = "test/test-sample";
            //// set html <title></title>
            this.setPageTitle("sample view");
        }

        async getContent(args) {
            return {
                avatar_url : "avatar_url",
                display_name: "songoku"
            }
        }
    }

    module.exports = SampleViewModel;
})();
