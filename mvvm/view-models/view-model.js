(function () {
    /** Abstract class ViewModel **/
    class ViewModel {
        constructor() {
            this.ViewName = "";
            this.Model = {
                user: {
                    token: null,
                    avatar_url: null,
                    name: ''
                },
                config: {
                    facebook_id: Configuration.FACEBOOK_ID,
                    google_id: Configuration.GOOGLE_ID,
                    gtm_id: Configuration.GTM_ID,
                    ga_id: Configuration.GA_ID,
                    hubspot_id: Configuration.HUBSPOT["hubspot_id"] || null,
                    plugin_view_360: Configuration.URLS['plugin_view_360'] || '',
                    plugin_view_3d: Configuration.URLS['plugin_view_3d'] || '',
                    quotation_url: Configuration.URLS['quotation_url'] || '',
                },
                base_url: BASE_URL || "/",
                page: {
                    title: "",
                    description: "",
                    image: "",
                    keywords: "",
                    metadata: []
                },
                content: {},
                Share: {
                }
            }
        }

        setUser(user) {
            if (user && user.avatar_url) {
                this.Model.user.avatar_url = user.avatar_url;
            }

            if (user && user.fullname) {
                this.Model.user.fullname = user.fullname;
            }

            if (user && user.token) {
                this.Model.user.token = user.token;
            }
        }

        setPageTitle(t) {
            this.Model.page.title = t;
        }

        setPageDescription(t) {
            this.Model.page.description = t;
        }

        setPageImage(t) {
            this.Model.page.image = t;
        }

        setPageKeywords(t) {
            this.Model.page.keywords = t;
        }

        getPageTitle() {
            return this.Model.page.title;
        }

        async load(args) {
            if (this.getContent && typeof this.getContent === "function") {
                this.Model.content = await this.getContent(args || {});
                return this.Model;
            }

            return this.Model;
        }
    }

    module.exports = ViewModel;

})();