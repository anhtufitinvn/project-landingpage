(function () {
    /** declare dependencies **/
    let Providers = require('../../server/providers');

    /** ViewMode is an abstract class  **/
    let ViewMode = require('./view-model');

    class HomeViewModel extends ViewMode {
        constructor(config) {
            super();
            //// set view template file : test/test-home.twig
            this.ViewName = config.view || "template_1/home/index";
            this.ProjectId = config.project_id || 32;
            this.Renovation = config.is_renovetion === true;
            this.RespondData = {};
            this.Model.GiftMobile = config.gift_mobile || 'static/gift/Default.gif';
            this.Model.base_url = config.base_url || '/';

            //// set html <title></title>
            this.setPageTitle("Thiết kế, cải tạo nội thất nguyên căn chỉ 24 giờ!");
            this.setPageDescription("Cùng FITIN lên ý tưởng cho thiết kế, cải tạo nội thất căn hộ của bạn. Đội ngũ chuyên gia uy tín. Thiết kế miễn phí 100%. Nhận ngay báo giá chi tiết. Tham khảo 3000+ sản phẩm nội thất trong nước và nhập khẩu.");
            this.setPageImage("https://cdn.fitin.vn/fitin/socials/fitin.jpg");
        }

        getContent(args) {
            return this.loadData();
        }

        async loadData() {
            if (this.Renovation === true) {
                let roomCategory = await Providers.ConsultantService.getRoomCategory();
                roomCategory = roomCategory.list || [];
                const blockRooms = [];
                let projectData = [];
                if ((roomCategory||[]).length) {
                    for (let index = 0; index < roomCategory.length; index++) {
                        const item = roomCategory[index];
                        let dataRooms = await Providers.ConsultantService.getLayoutRenovationByRoomType(item.type);
                        dataRooms = dataRooms.list || [];
                        if ((dataRooms||[]).length) {
                            projectData = projectData.concat(dataRooms);
                            blockRooms.push({
                                name: item.name,
                                type: item.type,
                                dataRooms
                            })
                        }
                    }
                }
                this.RespondData = {
                    blockRooms,
                    projectData
                };
                return {
                    blockRooms,
                    projectData
                }
            } else {
                let content = await Providers.ConsultantService.getProjectById(this.ProjectId);
                if (content) {
                    this.setPageTitle(`Thiết kế nội thất nguyên căn ${content.project_name} chỉ 24 giờ!`);
                    this.setPageDescription(`Cùng FITIN lên ý tưởng cho thiết kế nội thất căn hộ ${content.project_name} của bạn. Đội ngũ chuyên gia uy tín. Thiết kế miễn phí 100%. Nhận ngay báo giá chi tiết. Tham khảo 3000+ sản phẩm nội thất trong nước và nhập khẩu.`);
                    this.setPageImage(content.project_image_url);
                    this.RespondData = content;
    
                    const layouts = this.RespondData.layouts || [];
                    const bed_room_num_1 = layouts.filter( layout => layout.bed_room_num === 1 );
                    content.bed_room_num_1 = bed_room_num_1;
                    const bed_room_num_2 = layouts.filter( layout => layout.bed_room_num === 2 );
                    content.bed_room_num_2 = bed_room_num_2;
                    const bed_room_num_3 = layouts.filter( layout => layout.bed_room_num === 3 );
                    content.bed_room_num_3 = bed_room_num_3;
                    return content;
                }
            }

            return null;
        }

        getLayoutByRoomNumber(roomNumber) {
            const layouts = this.RespondData.layouts || [];
            let layoutByRoomNumber = layouts.filter( layout => layout.bed_room_num === roomNumber );
            this.Model.content.roomNumber = roomNumber;
            this.Model.content.layoutByRoomNumber = layoutByRoomNumber;
        }

        getLayoutDetail(layout_id) {
            const layouts = this.RespondData.layouts || [];
            let layoutDetail = layouts.filter( layout => layout.layout_id === layout_id );
            this.Model.layoutDetail = layoutDetail[0] || {};
        }

        getLayoutRenovationDetail(layout_id) {
            const layouts = this.RespondData.projectData || [];
            let layoutDetail = layouts.filter( layout => layout.int_id === layout_id );
            this.Model.layoutDetail = layoutDetail[0] || {};
        }

        getLayoutByRoomType(room_type) {
            const blockRooms = this.RespondData.blockRooms || [];
            let roomDetail = blockRooms.filter( room => room.type === room_type );
            this.Model.roomDetail = roomDetail[0] || {};
            this.Model.roomOther = blockRooms.filter( room => room.type !== room_type );
        }
    }

    module.exports = HomeViewModel;
})();
