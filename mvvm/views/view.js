(function () {
    /** Load dependencies **/
    let {debug} = require('../../core/logger');

    /** The View of Controller **/
    function View() {
        this.Response = null;
    }

    View.prototype.render = function (template, content) {
        if (this.Response && template) {
            this.Response.render(template, content || {});
        }
    };

    View.prototype.renderViewModel = function (vm) {
        if (this.Response && vm) {
            debug(JSON.stringify(vm.Model));
            this.Response.locals = vm.Model.user || null;
            this.Response.render(vm.ViewName, vm.Model);
        }
    };

    module.exports = View;
})();