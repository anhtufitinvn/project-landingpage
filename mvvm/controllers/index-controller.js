(function () {
    /** load dependencies of this controller **/
    let ProjectViewModel = require('../view-models/project-view-model');

    let BaseController = require('./base-controller');

    /** init Controller **/
    class IndexController extends BaseController {
        constructor() {
            super();
        }

        detectURL(req) {
            let template = {
                base_url: '/',
                view: "template_3",
                project_id: 27,
                gift_mobile: "static/gift/Default.gif"
            };
            const map_domain = Configuration.FITIN["map_domain"];
            if (map_domain[req.headers.host]) {
                template = map_domain[req.headers.host];
            }
            return template;
        }

        async indexAction(req, res) {
            /** use function View.renderViewModel(viewModel); **/
            const template = this.detectURL(req);
            const VM = new ProjectViewModel({
                ...template,
                view: template.view + "/home/index",
                is_renovetion: template.view === 'template_3'
            })
            ///// wait ViewModel load content
            await VM.load();
            //// render View with ViewModel data
            this.View.renderViewModel(VM);
        }
        async designAction(req, res) {
            /** use function View.renderViewModel(viewModel); **/
            const template = this.detectURL(req);
            const number_bed_room = parseInt(req.params.number_bed_room);
            const VM = new ProjectViewModel({
                ...template,
                view: template.view + "/design/index",
            })
            ///// wait ViewModel load content
            await VM.load();
            VM.getLayoutByRoomNumber(number_bed_room);
            //// render View with ViewModel data
            this.View.renderViewModel(VM);
        }
        async detailAction(req, res) {
            /** use function View.renderViewModel(viewModel); **/
            const template = this.detectURL(req);
            const layout_id = parseInt(req.params.layout_id);
            const VM = new ProjectViewModel({
                ...template,
                view: template.view + "/detail/index",
            })
            ///// wait ViewModel load content
            await VM.load();
            VM.getLayoutDetail(layout_id);
            //// render View with ViewModel data
            VM.Model.defaultImage = VM.Model.layoutDetail.image_url || 'https://cdn.fitin.vn/cms-ecom/images/2020/03/26/bedroom-1-1585210439.jpg';
            if (VM.Model.layoutDetail.gallery && VM.Model.layoutDetail.gallery.length) {
                VM.Model.defaultImage = VM.Model.layoutDetail.gallery[0].image_path_url
            }
            this.View.renderViewModel(VM);
        }
        // Renovation
        async detailRenovationAction(req, res) {
            /** use function View.renderViewModel(viewModel); **/
            const template = this.detectURL(req);
            const layout_id = parseInt(req.params.layout_id);
            const room_type = req.params.room_type;
            const VM = new ProjectViewModel({
                ...template,
                view: template.view + "/detail/index",
                is_renovetion: true
            })
            ///// wait ViewModel load content
            await VM.load();
            VM.getLayoutByRoomType(room_type);
            VM.getLayoutRenovationDetail(layout_id);
            //// render View with ViewModel data
            VM.Model.defaultImage = VM.Model.layoutDetail.layout_image_url || 'https://cdn.fitin.vn/cms-ecom/images/2020/03/26/bedroom-1-1585210439.jpg';
            if (VM.Model.layoutDetail.gallery && VM.Model.layoutDetail.gallery.length) {
                VM.Model.defaultImage = VM.Model.layoutDetail.gallery[0].image_url
            }
            this.View.renderViewModel(VM);
        }
        async roomRenovationAction(req, res) {
            /** use function View.renderViewModel(viewModel); **/
            const template = this.detectURL(req);
            const room_type = req.params.room_type;
            const VM = new ProjectViewModel({
                ...template,
                view: template.view + "/design/index",
                is_renovetion: true
            })
            ///// wait ViewModel load content
            await VM.load();
            VM.getLayoutByRoomType(room_type);
            //// render View with ViewModel data
            this.View.renderViewModel(VM);
        }

    }

    module.exports = IndexController;
})();