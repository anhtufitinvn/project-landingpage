(function () {
    /** load dependencies of this controller **/

    let BaseController = require('./base-controller');
    let GuideViewModel = require('../view-models/guide-view-model');
    let Providers = require('../../server/providers');

    /** init Controller **/

    class GuidesController extends BaseController {
        constructor() {
            super();
            this.VM = new GuideViewModel();
        }

        async indexAction(req, res) {
            //// load data to view-model if needed
            await this.VM.load();
            this.debug
            this.View.renderViewModel(this.VM);
        }

    }


    module.exports = GuidesController;
})();