(function () {
    /** load dependencies of this controller **/

    let BaseController = require('./base-controller');
    let SimpleViewModel = require('../view-models/sample-view-model');
    let Providers = require('../../server/providers');

    /** init Controller **/

    class SampleController extends BaseController {
        constructor() {
            super();
            this.VM = new SimpleViewModel();
        }

        async indexAction(req, res) {
            //// load data to view-model if needed
            ///// wait ViewModel load content
            await this.VM.load();

            //// render View with ViewModel data
            this.View.renderViewModel(this.VM);
        }

        async sampleAction(req, res) {
            //// load data to view-model if needed
            ///// wait ViewModel load content
            await this.VM.load();

            //// render View with ViewModel data
            this.View.renderViewModel(this.VM);
        };
    }


    module.exports = SampleController;
})();