(function () {
    const View = require('../views/view');
    const Handler = require('../../core/handler');
    class BaseController extends Handler {
        constructor(){
            super();
            this.View = new View();
            this.Request = null;
            this.Response = null;
        }

        async handle(req, res, action) {
            this.Request = req;
            //// set Response to Controller
            this.Response = res;
            //// set Response to View
            this.View.Response = res;
            if(action && this[action]) {
                return this[action](req, res);
            }

            throw new Error(`${action} not found`);
        };

    }


    module.exports = BaseController;
})();