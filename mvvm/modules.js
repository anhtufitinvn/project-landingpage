(function () {
    /**
     * Inject all controllers
     */
    let IndexController = require('./controllers/index-controller');
    let SampleController =  require('./controllers/sample-controller');
    let GuideController =  require('./controllers/guides-controller');
    module.exports = {
      Index: new IndexController(),
      Sample: new SampleController(),
      Guides: new GuideController(),
    };
})();