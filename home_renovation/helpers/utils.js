import qs from 'query-string';
export const queryConvert = function(){
  var queryStr = window.location.search,
    queryArr = queryStr.replace('?','').split('&'),
    queryParams = {};

  for (var q = 0, qArrLength = queryArr.length; q < qArrLength; q++) {
      var qArr = queryArr[q].split('=');
      queryParams[qArr[0]] = qArr[1];
  }

  return queryParams;
};

export const objectConvert = function (obj) {
  return qs.stringify(obj);
};

export const getURLParam = (key,target) => {
  var values = [];
  if(!target) {
    target = window.location.href;
  }

  // eslint-disable-next-line no-useless-escape
  key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

  var pattern = key + '=([^&#]+)';
  var o_reg = new RegExp(pattern,'ig');
  // eslint-disable-next-line no-constant-condition
  while (true){
    var matches = o_reg.exec(target);
    if (matches && matches[1]){
      values.push(matches[1]);
    } else {
      break;
    }
  }

  if (!values.length){
      return null;   
  } else {
      return values.length == 1 ? values[0] : values;
  }
}

export const serialize = function(obj, prefix) {
  var str = [],
    p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p,
        v = obj[p];
      str.push((v !== null && typeof v === "object") ?
        serialize(v, k) :
        encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
};

export const getSKU = function (file) {
  return file.split('.').slice(0, -1).join('.');
}
export const generateKeyname = function (brand = '', name = '') {
  if (name && brand) {
    return brand + '-' + name.toLowerCase().replace(/ /g, "_");
  } else {
    return '';
  }
}
export const getFileNameFromURL = function (url) {
  if (url) {
    return url.substring(url.lastIndexOf('/')+1);
  } else {
    return '';
  }
}

export const radiansToDegrees = (radians) => {
  var pi = Math.PI;
  return radians * (180/pi);
};
export const radiansToDegreesObject = (object) => {
  return {
    x: radiansToDegrees(object.x || 1),
    y: radiansToDegrees(object.y || 1),
    z: radiansToDegrees(object.z || 1),
  };
};

export const degreesToRadians = (degrees) => {
  var pi = Math.PI;
  return degrees * (pi/180);
};
export const degreesToRadiansObject = (object) => {
  return {
    x: degreesToRadians(object.x || 1),
    y: degreesToRadians(object.y || 1),
    z: degreesToRadians(object.z || 1),
  };
};