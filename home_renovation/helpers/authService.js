import Cookie from "../helpers/sessionHelper";
const isAuthenticated = () => !!Cookie.get('fid');

const getToken = () => {
  const token = Cookie.get('fid');
  return token != null ? token : null;
};

const getTokenGuest = () => {
  const token = Cookie.get('guest_fid');
  return token != null ? token : null;
};

const authService = {
  isAuthenticated,
  getToken,
  getTokenGuest
};

export default authService;
