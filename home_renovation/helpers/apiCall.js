import axios from 'axios';
import authService from './authService';
import { get } from "lodash";

const instance = axios.create();
export default function apiCall(method, url, data, params) {
  // config 
  const options = {
    method,
    url,
    data,
    params
  };

  // auth
  const isAuth = authService.isAuthenticated();
  if (isAuth) {
    options.headers = {
      Authorization: `Bearer ${authService.getToken()}`
    };
  } else {
    const guestToken = authService.getTokenGuest();
    if (guestToken) {
      options.url = options.url + (options.url.indexOf('?') >= 0 ? "&" : "?") + "FITIN_GUEST_TOKEN=" + guestToken;
    }
  }

  return instance(options).then(response => {
    if (response && get(response, 'data.code') === 0) {
      return { response: response.data };
    } else {
      Promise.reject(response.data);
    }
  }).catch(error => {
    Promise.reject(error);
  });
}
