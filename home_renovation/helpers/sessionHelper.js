/* eslint-disable */
// require('./store.legacy.min.js');
import Cookie from 'js-cookie'
export default {
  get: (name) => {
    return Cookie.get(name);
  },
  set: (name, data) => {
    return Cookie.set(name, data);
  },
  remove: (name) => {
    return Cookie.remove(name);
  }
};
