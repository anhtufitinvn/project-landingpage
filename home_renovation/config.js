export const hsID = process.env.VUE_APP_HS_ID || '7117918';
// URL
export const URL_ECOM = process.env.VUE_APP_ECOM_URL || 'https://fitin.vn';
export const URL_ECOM_EDITOR = URL_ECOM + '/editor/';

export const UrlBase = process.env.BASE_URL;
export const UrlLogin = UrlBase + 'login'

export const SEOTitle = process.env.VUE_APP_SEOTITLE || 'FITIN.VN - Nền tảng mua sắm nội thất Online Uy Tín và Chất Lượng';
export const SEODesc = process.env.VUE_APP_SEODESC || 'FITIN.VN là nền tảng mua sắm nội thất Online ứng dụng công nghệ cao tại Việt Nam. Chuyên cung cấp nội thất trọn gói cho căn hộ, đồ trang trí đến từ các thương hiệu uy tín, đảm bảo chất lượng. Nhận tư vấn thiết kế thi công nội thất trọn gói.';

// api Ecom
export const ApiEcomBase = process.env.VUE_APP_API_ECOM_URL || 'https://api2.fitin.dev';
export const ApiLogin = ApiEcomBase + '/api/frontend/v1/login-v2';
// api consultant
export const ApiConsultantBase = process.env.VUE_APP_API_TUVAN_URL || 'https://tuvan.fitin.dev';
export const ApiProject =  ApiConsultantBase + '/api/v2/projects?project_id=';
export const ApiLayoutRenovationByRoomType =  ApiConsultantBase + '/api/v2/renovation/layouts?page=1&room_type=';
export const ApiLayoutRenovationDetail =  ApiConsultantBase + '/api/v2/renovation/layouts/';
export const ApiSubmitHubspot =  ApiConsultantBase + '/api/v1/hubspot/form/quick/submit';
export const ApiResources =  ApiConsultantBase + '/api/v1/resources/ct-forms';
export const ApiRoomCategory =  ApiConsultantBase + '/api/v2/resources/room-categories';
