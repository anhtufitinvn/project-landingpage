import { UrlLogin } from '@/config'

export default function ({ store, redirect }) {
  // If the user is not authenticated
  if (!store.state.access_token) {
    return redirect(UrlLogin)
  }
}
