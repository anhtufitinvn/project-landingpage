#!/bin/bash
DATE=$(date +"%Y.%m.%d-%H%M")
VERSION="v.${DATE}"
echo "${VERSION}"
echo 'cd to WorkPlace'
echo "${PWD}"
echo 'Done cd to WorkPlace'

echo 'generate FE'
npm run generate:production
echo 'Done generate FE'

echo 'cd to Build'
cd build-production
echo 'Done cd to Unity'

echo 'zip file'
tar cvf "${VERSION}.tar" ./
echo 'zip file'

echo 'cp to Server'
rsync -avzhe "ssh -p 2222" "${VERSION}.tar" root@sliveweb1.fitin.link:/data/php/html.fitin.vn/public/caitaonoithat
rsync -avzhe "ssh -p 2222" "${VERSION}.tar" root@sliveweb1.fitin.link:/data/php/html.fitin.vn/public/caitaonoithat/build.tar
echo 'cp to Server'

echo 'unzip on Server'
ssh -t 'root@sliveweb1.fitin.link' -p 2222 'cd "/tmp" && cd "/data/php/html.fitin.vn/public/caitaonoithat" && tar xf build.tar ./ && chown -R apache:root ./ && chmod -R 755 ./;'

# content of your script
