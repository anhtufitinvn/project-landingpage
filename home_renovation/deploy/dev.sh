#!/bin/bash
DATE=$(date +"%Y.%m.%d-%H%M")
VERSION="v.${DATE}"
echo "${VERSION}"
echo 'cd to WorkPlace'
echo "${PWD}"
echo 'Done cd to WorkPlace'

echo 'generate FE'
npm run generate:dev
echo 'Done generate FE'

echo 'cd to Build'
cd build-dev
echo 'Done cd to Unity'

echo 'zip file'
tar cvf "${VERSION}.tar" ./
echo 'zip file'

echo 'cp to Server'
rsync -avzhe ssh "${VERSION}.tar" root@45.124.94.203:/data/php/html.fitin.dev/public/caitaonoithat
rsync -avzhe ssh "${VERSION}.tar" root@45.124.94.203:/data/php/html.fitin.dev/public/caitaonoithat/build.tar
echo 'cp to Server'

echo 'unzip on Server'
ssh -t 'root@45.124.94.203' 'cd "/tmp" && cd "/data/php/html.fitin.dev/public/caitaonoithat" && tar xf build.tar ./ && chown -R apache:root ./ && chmod -R 755 ./;'

# content of your script
