// import axios from 'axios'
import {
  ApiResources
} from "@/config";
import apiCall from "../helpers/apiCall";
import handleErrorController from "@/helpers/handleErrorController";
import { get } from "lodash";
export const state = () => ({
  data: null
});

export const mutations = {
  SET_RESOURCE(state, data) {
    state.data = data;
  }
};

export const getters = {
  getResource(state) {
    return state.data;
  }
};

export const actions = {
  async getApiResource({ commit }) {
    try {
      const { response } = await apiCall("get", ApiResources);
      if (response.code === 0) {
        commit("SET_RESOURCE", get(response, 'data', {}));
      } else {
        handleErrorController(response);
      }
      return get(response, 'data', []);
    } catch (error) {
      handleErrorController(error);
    }
  }
};
