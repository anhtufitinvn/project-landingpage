import axios from "axios";
import apiCall from "../helpers/apiCall";
import {
  ApiLogin,
} from "../config";

import handleErrorController from "@/helpers/handleErrorController";
import { get } from "lodash";

// const Cookie = process.client ? require("js-cookie") : undefined;
import Cookie from "../helpers/sessionHelper";
// const cookieparser = process.server ? require('cookieparser') : undefined;
// if (cookieparser) {
//   cookieparser.maxLength=8192;
// }

export const strict = false;

export const state = () => ({
  access_token: process.client ? Cookie.get("fid") : null,
  customer_id: process.client ? Cookie.get("uid") : null,
  authUser: null,
  guest_token: null,
  authEmail: null,
  authOTP: null
});

export const getters = {
  access_token(state) {
    return state.access_token;
  },
  guest_token(state) {
    return state.guest_token;
  }
};

export const mutations = {
  SET_USER(state, user) {
    state.authUser = user;
    state.access_token = user != null ? user.access_token : null;
    state.customer_id = user != null ? user.customer_id : null;
  },
  SET_TOKEN(state, access_token) {
    state.access_token = access_token;
  },
  SET_GUEST_TOKEN(state, guest_token) {
    state.guest_token = guest_token;
  },
};

function handleAfterLogin(dispatch, data) {
  Cookie.set("fid", data.data.access_token);
  Cookie.set("uid", data.data.customer_id);
  axios.defaults.headers.common[
    "Authorization"
  ] = `Bearer ${data.data.access_token}`;
  // dispatch('profile/getProfile', {root : true});
  // dispatch('cart/getApiGetCartTotal', {root : true});
  // dispatch('wishlist/getApiWishlistID', {root : true});
}
function handleAfterLogout(commit) {
  commit("SET_USER", null);
  // commit("cart/SET_CART_TOTAL", 0, { root: true });
  Cookie.remove("fid");
  axios.defaults.headers.common["Authorization"] = null;
}
export const actions = {
  // nuxtServerInit is called by Nuxt.js before server-rendering every page
  // nuxtServerInit ({ commit }, { req }) {
  //   if (req && req.headers && req.headers.cookie) {
  //     const parsed = cookieparser.parse(req.headers.cookie);
  //     try {
  //       commit('SET_TOKEN', {access_token: parsed.fid});
  //     } catch (err) {
  //       // No valid cookie found
  //     }
  //   }
  // },

  async postApiLogin({ commit, dispatch }, { email, password }) {
    try {
      const { response } = await apiCall("post", ApiLogin, { email, password });
      if (response.code === 0) {
        commit("SET_USER", response.data);
        handleAfterLogin(dispatch, response);
      } else {
        handleAfterLogout(commit);
        handleErrorController(response);
      }
      return response;
    } catch (error) {
      handleErrorController(error);
    }
  },
  
};
