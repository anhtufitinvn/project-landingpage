// JS import
import Vue from 'vue';

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);

import {
  hsID
} from "@/config";
const script = document.createElement("script");
script.src = "https://js.hs-scripts.com/" + hsID + ".js";
document.body.appendChild(script);
script.addEventListener("load", () => {
  function onConversationsAPIReady() {
    console.log(`HubSpot Conversations API: ${window.HubSpotConversations}`, window.HubSpotConversations);
  }

  /*
   If external API methods are already available, use them.
  */
  if (window.HubSpotConversations) {
    onConversationsAPIReady();
  } else {
    /*
      Otherwise, callbacks can be added to the hsConversationsOnReady on the window object.
      These callbacks will be called once the external API has been initialized.
    */
    window.hsConversationsOnReady = [onConversationsAPIReady];
  }

  const target = (window.location.hash && window.location.hash.substr(1)) || null;
  if (target) {
    const targetEle = document.getElementById(target);
    targetEle.scrollIntoView();
  }
});
