import { repeat } from "lodash";
function toCurrency(value) {
  // if (typeof value !== "number") {
  //   value = parseInt(value || 0);
  // }
  // var formatter = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' });
  // return formatter.format(parseInt(value));

  if (typeof value !== "number") {
    value = parseInt(value || 0);
  }
  return value.toLocaleString(
    undefined, // leave undefined to use the browser's locale,
               // or use a string like 'en-US' to override it.
    { minimumFractionDigits: 0 }
  );
}


const processNameForParent = (item) => {
  return repeat('--', item.depth) +  ' '  + item.name;
};


export default {
  toCurrency: toCurrency,
  processNameForParent
}



