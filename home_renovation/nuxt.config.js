const SEOTitle = process.env.VUE_APP_SEOTITLE || 'Thiết kế cải tạo phòng chức năng chỉ 24 giờ!';
const SEODesc = process.env.VUE_APP_SEODESC || 'Cùng FITIN lên ý tưởng cho thiết kế cải tạo phòng của bạn. Đội ngũ chuyên gia uy tín. Thiết kế miễn phí 100%. Nhận ngay báo giá chi tiết. Tham khảo 3000+ sản phẩm nội thất trong nước và nhập khẩu.';
const BUILD_VERSION = (new Date()).toString();
const LRUCache = require('lru-cache');
import axios from 'axios';
import { get } from "lodash";
import { UrlBase, ApiRoomCategory, ApiLayoutRenovationByRoomType } from "./config";

module.exports = {
  mode: 'universal', // universal || spa
  scrollToTop: true,
  // animation
  layoutTransition: {
    name: 'layout',
    mode: 'out-in'
  },
  /*
   ** Headers of the page
   */
  head: {
    title: SEOTitle,
    titleTemplate: `%s | Fitin.vn`,
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
      },
      {
        name: 'mobile-web-app-capable',
        content: 'yes'
      },
      {
        hid: 'description',
        name: 'description',
        content: SEODesc
      },
      {
        hid: 'robots',
        name: 'robots',
        content: 'index,follow'
      },
      {
        name: 'apple-mobile-web-app-capable',
        content: 'yes'
      },
      {
        name: 'cache-control',
        content: 'max-age=0'
      },
      {
        name: 'cache-control',
        content: 'no-cache'
      },
      {
        name: 'expires',
        content: '0'
      },
      {
        name: 'pragma',
        content: 'no-cache'
      },
      {
        name: 'version',
        content: BUILD_VERSION
      },
      {
        hid: 'twitter:card',
        name: 'twitter:card',
        content: "summary"
      },
      {
        hid: 'twitter:site',
        name: 'twitter:site',
        content: 'FITIN.vn'
      },
      {
        hid: 'twitter:creator',
        name: 'twitter:creator',
        content: 'FITIN.vn'
      },
      {
        hid: 'twitter:title',
        name: 'twitter:title',
        content: SEOTitle
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content: SEODesc
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content: 'https://saigonsouthresidences.fitin.vn/_nuxt/img/a144229.jpg'
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: 'https://saigonsouthresidences.fitin.vn/_nuxt/img/a144229.jpg'
      },
      {
        hid: 'og:site_name',
        name: 'og:site_name',
        content: 'FITIN.vn'
      },
      {
        hid: 'og:title',
        name: 'og:title',
        content: SEOTitle
      },
      {
        hid: 'og:description',
        name: 'og:description',
        content: SEODesc
      }
    ],
    link: [{
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      },
      {
        rel: 'apple-touch-icon',
        href: '/fitinin-appicon.png'
      },
      {
        rel: 'apple-touch-startup-image',
        href: '/fitinin-appicon.png'
      },
    ],
    // Support polyfill for browsers that don't support it (Safari Mobile for example)
    script: [{
      src: 'https://polyfill.io/v2/polyfill.min.js?features=IntersectionObserver',
      body: true
    },{
      src: UrlBase + 'libs/tiny-slider/tiny-slider.js',
      body: true
    }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#94d4f7',
    failedColor: 'red',
    height: '5px'
  },
  /*
   ** Global CSS
   */
  css: [
    '@/static/libs/tiny-slider/tiny-slider.css', 
    '@/assets/scss/index.scss'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    // { src: '@/plugins/ga.js', mode: 'client' },
    {
      src: '@/plugins/hotjar.js',
      mode: 'client'
    },
    // {
    //   src: '@/plugins/vue-carousel.js',
    //   mode: 'client'
    // },
    // {
    //   src: 'plugins/owl-carousel.js',
    //   ssr: false
    // },
    {
      src: '@/plugins/gtm.js',
      mode: 'client'
    }, {
      src: '@/plugins/client.js',
      mode: 'client'
    },
    {
      src: '@/plugins/server.js',
      mode: 'server'
    },
    '@/plugins/all.js',
    '@/plugins/i18n.js'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    // '@nuxtjs/stylelint-module',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    // Doc: https://github.com/nuxt-community/device-module
    // 'nuxt-device-detect',

    // With options
    ['@nuxtjs/component-cache', {
      max: 10000,
      maxAge: 1000 * 60 * 60
    }],

    // [
    //   "nuxt-imagemin",
    //   {
    //     optipng: { optimizationLevel: 5 },
    //     gifsicle: { optimizationLevel: 2 }
    //   }
    // ]

    // https://auth.nuxtjs.org/recipes/extend.html
    // "@nuxtjs/auth"

    // https://github.com/nuxt-community/sitemap-module#routes-1
    // "@nuxtjs/sitemap"

    //  https://www.npmjs.com/package/@nuxtjs/style-resources
    // '@nuxtjs/style-resources'
  ],

  env: {
    ...process.env
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    // parallel: true,
    // terser: true,
    // friendlyErrors: false,
    // analyze: {
    //   analyzerMode: 'static'
    // },
    // cache: true,

    extractCSS: true,
    optimization: {
      runtimeChunk: true,
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true
          }
        }
      }
    },
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.devtool = '#source-map';
      }
      if (
        config.optimization.splitChunks &&
        typeof config.optimization.splitChunks === 'object'
      ) {
        config.optimization.splitChunks.maxSize = 200000;
      }
    },
  },
  /*
   ** Generate configuration
   */
  generate: {
    // fallback: true,
    interval: 10,
    async routes (callback) {
      const routes = [];
      const roomCategory = await axios.get(ApiRoomCategory);
      if (get(roomCategory, 'data.data.list', []).length) {
        for (let index = 0; index < get(roomCategory, 'data.data.list', []).length; index++) {
          const item = get(roomCategory, 'data.data.list.'+index, {});
          routes.push(`/${item.type}`);
          const dataRooms = await axios.get(ApiLayoutRenovationByRoomType + item.type);
          if (get(dataRooms, 'data.code') === 0) {
            get(dataRooms, 'data.data.list', []).map(i => {
              routes.push(`/${item.type}/${i.int_id}`)
            })
          }
        }
      }
      callback(null, routes);
    }
  },
  router: {
    // To disable prefetching, uncomment the line
    // prefetchLinks: false

    // Activate prefetched class (default: false)
    // Used to display the check mark next to the prefetched link
    linkPrefetchedClass: 'nuxt-link-prefetched',

    base: process.env.BASE_URL
  },
  render: {
    bundleRenderer: {
      cache: new LRUCache({
        max: 1000,
        maxAge: 1000 * 60 * 15
      })
    }
  }
};